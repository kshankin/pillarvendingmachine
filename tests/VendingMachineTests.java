import static com.kshankin.vendingmachine.VendingMachine.COINS.DIME;
import static com.kshankin.vendingmachine.VendingMachine.COINS.NICKEL;
import static com.kshankin.vendingmachine.VendingMachine.COINS.PENNY;
import static com.kshankin.vendingmachine.VendingMachine.COINS.QUARTER;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.kshankin.vendingmachine.ProductStack;
import com.kshankin.vendingmachine.VendingMachine;

public class VendingMachineTests {
	private VendingMachine vMachine;

	@Before
	public void setUp() throws Exception {
		vMachine = new VendingMachine();
	}

	@Test
	public void whenNoCoinsAreInserted() {
		vMachine.availableBalance = 0.0;
		assertEquals("INSERT COIN", vMachine.getDisplay());
	}

	@Test
	public void WhenPennyIsInserted() {
		assertEquals("pennies are not accepted", vMachine.insertCoin(PENNY));
	}
	
	@Test
	public void whenTheDisplayIsChecked(){
		vMachine.availableBalance=0.65;
		assertEquals("$0.65", vMachine.getDisplay());
	}

	@Test
	public void whenAValidCoinIsInserted() {
		vMachine.availableBalance = 0.0;
		assertEquals("$0.05", vMachine.insertCoin(NICKEL));
		assertEquals("$0.15", vMachine.insertCoin(DIME));
		assertEquals("$0.40", vMachine.insertCoin(QUARTER));
	}

	@Test
	public void whenACustomerSelectsAProductButThereIsNotEnoughMoneyInTheAvailableBalance() {
		assertEquals("PRICE: $1.00", vMachine.purchaseProduct(1));
		assertEquals("PRICE: $0.50", vMachine.purchaseProduct(2));
		assertEquals("PRICE: $0.65", vMachine.purchaseProduct(3));
	}

	@Test
	public void whenACustomerSelectsAColaAndEnoughMoneyIsAvailable() {
		vMachine.availableBalance = 1.0;
		assertEquals("THANK YOU", vMachine.purchaseProduct(1));
	}

	@Test
	public void whenACustomerSelectsChipsAndEnoughMoneyIsAvailable() {
		vMachine.availableBalance = 0.50;
		assertEquals("THANK YOU", vMachine.purchaseProduct(2));
	}

	@Test
	public void whenACustomerSelectsCandyAndEnoughMoneyIsAvailable() {
		vMachine.availableBalance = 0.65;
		assertEquals("THANK YOU", vMachine.purchaseProduct(3));
	}

	@Test
	public void whenAPurchaseHasBeenMadeAndNoMoneyIsLeftOver() {
		vMachine.availableBalance = 0.0;
		whenACustomerSelectsCandyAndEnoughMoneyIsAvailable();
		assertEquals("INSERT COIN", vMachine.getDisplay());
	}

	@Test
	public void whenAPurchaseHasBeenMadeAndMoneyIsLeftOver() {
		vMachine.availableBalance = 1.65;
		vMachine.purchaseProduct(1);
		assertEquals("INSERT COIN", vMachine.getDisplay());
	}
	
	@Test
	public void whenCoinsNeedToBeReturned(){
	   vMachine.availableBalance =0.65;
	   assertEquals("0.65", vMachine.dispenseChange());
	   assertEquals("0.0", vMachine.availableBalance.toString());
	}
	
	@Test
	public void whenMachineNeedsToMakeChange(){
		vMachine.availableBalance = 1.65;
		vMachine.purchaseProduct(1);
		assertEquals(true, vMachine.changeToReturn==0.65);
	}
	
	@Test
	public void whenProductIsSoldOut(){
		vMachine.inventory.put(3, new ProductStack("Candy", 0.65, 0));
		assertEquals("SOLD OUT", vMachine.purchaseProduct(3));
	}
	
	@Test
	public void whenCustomerEjectsChange(){
		vMachine.availableBalance =1.25;
		assertEquals("INSERT COIN", vMachine.returnCoins());
	}
	
	@Test
	public void whenExactChangeIsNeeded(){
	    vMachine.exactChangeOnly = true;
	    assertEquals("EXACT CHANGE ONLY", vMachine.getDisplay());
	}

}
