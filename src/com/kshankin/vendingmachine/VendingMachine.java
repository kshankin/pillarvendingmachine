package com.kshankin.vendingmachine;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.HashMap;

public class VendingMachine {

	public static enum COINS {
		PENNY, NICKEL, DIME, QUARTER
	};

	public static enum PRODUCTS {
		COLA, CHIPS, CANDY;
	}

	public Double availableBalance = 0.0;
	public boolean exactChangeOnly = false;
	public Double changeToReturn;
	public HashMap<Integer, ProductStack> inventory;
	NumberFormat formatter = NumberFormat.getCurrencyInstance();

	public VendingMachine() {
		inventory = new HashMap<Integer, ProductStack>();
		stockMachine();
	}

	/**
	 * initializes the stock of the machine's products
	 */
	private void stockMachine() {
		inventory.put(1, new ProductStack("Cola", 1.00, 5));
		inventory.put(2, new ProductStack("Chips", 0.50, 3));
		inventory.put(3, new ProductStack("Candy", 0.65, 1));
	}

	/**
	 * Gets the display for the machine
	 * @return the display
	 */
	public String getDisplay() {
		if (availableBalance > 0.0)
			return formatter.format(availableBalance);
		if (exactChangeOnly) {
			return "EXACT CHANGE ONLY";
		} else {
			return "INSERT COIN";
		}
	}

	/**
	 * Adds the insertedCoin amount to the available balance.
	 * @param insertedCoin the coin the has been inserted
	 * @return available balance.
	 */
	public String insertCoin(COINS insertedCoin) {
		switch (insertedCoin) {
		case PENNY:
			return "pennies are not accepted";
		case NICKEL:
			availableBalance += 0.05;
			break;
		case DIME:
			availableBalance += 0.10;
			break;
		case QUARTER:
			availableBalance += 0.25;
			break;
		default:
			return "unrecognized currency type";
		}
		roundAvailableBalance();
		return getDisplay();
	}

	/**
	 * Purchases a product from the machine
	 * @param selection number of the item to purchase
	 * @return "SOLD OUT" if the item is sold out, the items price if the availableBalance is less than the cost of the product, "THANK YOU" if the product has been dispensed.
	 */
	public String purchaseProduct(int selection) {
		ProductStack pStack = inventory.get(selection);
		if (pStack.count == 0) {
			return "SOLD OUT";
		}
		if (availableBalance < pStack.price) {
			return "PRICE: " + formatter.format(pStack.price);
		} else {
			availableBalance -= pStack.price;
			
			roundAvailableBalance();
			dispenseChange();
			
			return "THANK YOU";
		}
	}

	/**
	 * Returns the available balance to the customer
	 * @return The display "INSERT COIN"
	 */
	public String returnCoins() {
		dispenseChange();
		return getDisplay();
	}

	/**
	 * Makes and dispenses change.
	 * @return The amount of change returned.
	 */
	public String dispenseChange() {
		changeToReturn = new Double(availableBalance);
		availableBalance = 0.0;
		return changeToReturn.toString();
	}

	/**
	 * Adjusts for doing arithmetic with doubles
	 */
	private void roundAvailableBalance() {
		availableBalance = new BigDecimal(availableBalance).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}

}
