package com.kshankin.vendingmachine;

public class ProductStack {
	public String name;
	public double price;
	public int count;

	public ProductStack(String name, double price, int count) {
		this.name = name;
		this.price = price;
		this.count = count;
	}
}
